// Select DOM elements to work with
const signInButton = document.getElementById('signIn');
const signOutButton = document.getElementById('signOut');
const titleDiv = document.getElementById('title-div');
const welcomeDiv = document.getElementById('welcome-div');
const tableDiv = document.getElementById('table-div');
const tableBody = document.getElementById('table-body-div');

function welcomeUser(username) {
  signInButton.classList.add('d-none');
  signOutButton.classList.remove('d-none');
  titleDiv.classList.add('d-none');
  welcomeDiv.classList.remove('d-none');
  welcomeDiv.innerHTML = `Welcome ${username}!`;
  document.getElementById('search-container').style.display = 'block';
}
