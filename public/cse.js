if (!window.__gcse) {
  window.__gcse = { parsetags: 'onload' };
}

function removeFrame() {
  const frame = document.getElementById('frame');
  if (frame) {
    frame.remove();
  }
}

function inputListener(e) {
  if (!e.target.value) {
    removeFrame();
  }
}

function addInputListener() {
  const inputs = document.getElementsByClassName('gsc-input');
  if (inputs.length) {
    inputs[0].removeEventListener('input', inputListener);
    inputs[0].addEventListener('input', inputListener);
  }
}

const params = new URLSearchParams(location.search);
const local = params.get('local');
let baseUrl = 'https://app.adenin.com';

if (local) {
  if (local.startsWith('http')) {
    baseUrl = local;
  } else {
    baseUrl = 'https://localhost:44367';
  }
}

function addFrame(token, query) {
  const frame = document.createElement('iframe');

  frame.id = 'frame';
  frame.src = `${baseUrl}/authenticate?token=${token.accessToken}&tokensrc=office-365&returnUrl=${
    encodeURIComponent(`${baseUrl}/app/embedcardframe?query=${query}`)
  }`;

  document
    .getElementById('frame-container')
    .appendChild(frame);

  iFrameResize({ log: true }, '#frame');
}

function searchCallback(layout, query) {
  removeFrame();
  addInputListener();

  myMSALObj
    .acquireTokenSilent({ account: myMSALObj.getAllAccounts()[0] })
    .then((token) => addFrame(token, query));
}

window.__gcse.searchCallbacks = {
  web: {
    starting: searchCallback,
  },
};
